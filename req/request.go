package req

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path"
	"strconv"
	"strings"
)

// BaseResponse TODO
type BaseResponse struct {
	Info string
	Code int
}

type irequired interface {
	Required() []string
}

type result struct {
	Stat int
	Info string
	Msg  string
}

const (
	deviceId = "dev_lblue"
	serverId = "6"
	traceId  = "dev"
	appId    = 1014
	token    = "dev"
	rid      = "4046910292469219329"
)

// Post perform post action
func Post(url string, APIID string, params map[string]interface{}, target interface{}) int {
	defaultParam := map[string]interface{}{"rid": rid, "deviceId": deviceId, "token": token, "traceId": traceId, "appId": appId, "actionId": APIID, "serverId": serverId}
	for k, v := range params {
		defaultParam[k] = v
	}
	jsonStr, err := json.Marshal(defaultParam)
	if err != nil {
		panic(fmt.Errorf("invalid parameters %v", defaultParam))
	}
	fmt.Printf("request parameters are: \n%#v\n", defaultParam)
	fmt.Printf("json str is %s", jsonStr)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("X-Custom-Header", "test")
	req.Header.Set("Content-Type", "application/json")
	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	// io.Copy(os.Stdout, resp.Body)
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Print(err)
	}
	fmt.Printf("\nsource json body is: %s\n", string(body))
	stat := new(result)
	err = json.Unmarshal(body, stat)
	if err != nil {
		panic(err)
	}
	err = json.Unmarshal(body, target)
	if err != nil {
		panic(err)
	}
	return stat.Stat
	// err = json.NewDecoder(resp.Body).Decode(target)
}

// InArray check key is in list or not
func InArray(key string, list *[]string) bool {
	for _, k := range *list {
		if key == k {
			return true
		}
	}
	return false
}

// GetActionID get all action id list in maester-go
func GetActionID() []string {
	proot := getProjectRoot()
	proot = path.Join(proot, "app", "actions", "area")
	dirs := ListDir(proot, true, true)
	var actionID []string
	for _, dir := range dirs {
		files := ListDir(dir, false, false)
		for _, file := range files {
			length := len(file)
			if file[length-7:length-3] == "test" {
				continue
			}
			if _, err := strconv.Atoi(file[:length-3]); err == nil {
				actionID = append(actionID, file[:length-3])
			}

		}
	}
	return actionID
}

func getProjectRoot() string {
	gopath := os.Getenv("GOPATH")
	for _, p := range strings.Split(gopath, ";") {
		p = path.Join(p, "src", "maester-go")
		if _, err := os.Stat(p); err != nil {
			// file not exists
			continue
		}
		return p
	}
	panic("No such project named maester-go in gopath")
}

// ListDir list directories and files in fpath
func ListDir(fpath string, fullPath bool, listDir bool) []string {
	files, err := ioutil.ReadDir(fpath)
	dirs := make([]string, len(files))
	fileName := ""
	if err != nil {
		log.Printf("list error path %s", fpath)
		log.Fatal(err)
	}
	for i, f := range files {
		if fullPath {
			fileName = path.Join(fpath, f.Name())
		} else {
			fileName = f.Name()
		}
		if f.IsDir() == listDir {
			dirs[i] = fileName
		}
	}
	return dirs
}

// CheckStat checking stat == 0 or not
func CheckStat(stat int) {
	if stat != 0 {
		panic(fmt.Errorf("invalid response stat %d", stat))
	}
}

// CheckCommon checking random visist parameters
func CheckCommon(url string, aid string) {
	demo := struct{}{}
	params := map[string]interface{}{"traceId": "test"}
	stat := Post(url, aid, params, demo)
	CheckStat(stat)
}
