package req

import "fmt"

// RandomVisit response key in random visit family
type RandomVisit struct {
	Friendship     int        `json:"friendship"`
	FamilyId       int        `json:"familyId"`
	OverTime       int        `json:"overTime"`
	MiracleTrigger int        `json:"miracleTrigger"`
	Event          [][4]int   `json:"event"`
	Children       []Children `json:"children"`
	ChildrenNum    int        `json:"childrenNum"`
}

type Children struct {
	Business   int
	ChildrenId string
	Exp        int
	Force      int
	Farm       int
	Politics   int
	NatureId   int
}

// CheckRandomVisit checking random visist parameters
func CheckRandomVisit(url string) {
	visit := new(RandomVisit)
	var aid = "10401"
	params := map[string]interface{}{"traceId": "test"}
	stat := Post(url, aid, params, visit)
	CheckStat(stat)
	fmt.Printf("\nresponse body is:%v\n", visit)
}
