package req

// EnterFamilyMap api 10411
type EnterFamilyMap struct {
	BaseResponse
	Events    []EventList
	UnlockTip []int
}

// EventList api 101411
type EventList struct {
	FamilyId int
	Npc      int
	NpcType  int
	Opera    int
}

// Required parameters to post
func (e *EnterFamilyMap) Required() []string {
	return []string{"Events", "UnlockTip"}
}

// Required parameters to post
func (e *EventList) Required() []string {
	return []string{"FamilyId", "Npc", "NpcType", "Opera"}
}
