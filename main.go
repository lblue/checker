package main

import (
	"checker/req"
	"flag"
	"fmt"
	"os"
)

var game = flag.Bool("game", false, "post forward to frontend")
var admin = flag.Bool("admin", false, "post forward to admin backend")
var aid = flag.String("aid", "", "action id to test")

const (
	postURL     = "http://localhost:10002"
	urlAdmin    = "admin"
	urlFrontend = "frontend"
)

func main() {
	flag.Parse()
	fullURL := ""
	if *admin {
		fullURL = postURL + "/" + urlAdmin
	}
	if *game {
		fullURL = postURL + "/" + urlFrontend
	}
	if fullURL == "" {
		fullURL = postURL + "/" + urlFrontend
	}
	if *aid == "" {
		flag.Usage()
		os.Exit(1)
	}
	aids := req.GetActionID()
	if !req.InArray(*aid, &aids) {
		panic(fmt.Errorf("%s not in %v", *aid, aids))
	}
	// invoke checker accounding to aid
	switch *aid {
	case "10401":
		req.CheckRandomVisit(fullURL)

	default:
		req.CheckCommon(fullURL, *aid)
	}

}
